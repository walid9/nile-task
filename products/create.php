<?php
if($_POST){

    // include database connection
    include '../database/config.php';

    try{

        // insert query
        $query = "INSERT INTO products SET name=:name, sub_id=:sub, image=:img";

        // prepare query for execution
        $stmt = $pdo->prepare($query);

        // posted values
        $name=htmlspecialchars(strip_tags($_POST['name']));
        $sub=htmlspecialchars(strip_tags($_POST['sub_id']));
        $imgFile = $_FILES['image']['name'];
        $tmp_dir = $_FILES['image']['tmp_name'];
        $imgSize = $_FILES['image']['size'];
        if(empty($imgFile)){
            $errMSG = "Please Select Image File.";
        }
        else
        {
            $upload_dir = 'images/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

            // valid image extensions
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions

            // rename uploading image
            $pic = rand(1000,1000000).".".$imgExt;

            // allow valid image file formats
            if(in_array($imgExt, $valid_extensions)){
                // Check file size '5MB'
                if($imgSize < 5000000)    {
                    move_uploaded_file($tmp_dir,$upload_dir.$pic);
                }
                else{
                    $errMSG = "Sorry, your file is too large.";
                }
            }
            else{
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            }
        }
        // bind the parameters
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':sub', $sub);
        $stmt->bindParam(':img', $pic);

        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }

    }

        // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Create Product</title>

    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

</head>
<body>

<!-- container -->
<div class="container">
    <div class="page-header">
        <h1>Create New Product</h1>
    </div>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
        <table class='table table-hover table-responsive table-bordered'>
            <tr>
                <td>Name</td>
                <td><input type='text' name='name' class='form-control' required></td>
            </tr>
            <tr>
                <td>Image</td>
                <td><input type="file" name='image' class='form-control' accept="image/*" required></td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <select name="sub_id" class="form-control" required>
                        <?php
                        // include database connection
                        include '../database/config.php';
                        $subs = "SELECT id, name FROM sub_categories ORDER BY id DESC";
                        $stamt = $pdo->prepare($subs);
                        $stamt->execute();
                        while ($row = $stamt->fetch(PDO::FETCH_ASSOC)){
                            extract($row);
                            echo "<option value='{$row['id']}'>{$row['name']}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Action</td>
                <td>
                    <input type='submit' value='Save' class='btn btn-primary' />
                    <a href='index.php' class='btn btn-danger'>Back to categories</a>
                </td>
            </tr>
        </table>
    </form>

</div> <!-- end .container -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>