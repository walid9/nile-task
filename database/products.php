<?php

require_once "config.php";

// create table categories

try{
    $sql = "CREATE TABLE products(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL UNIQUE,
        image VARCHAR(255) NOT NULL,
         sub_id int NOT NULL,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (sub_id) REFERENCES sub_categories(id)
    )";
    $pdo->exec($sql);
    echo "Table created successfully.";
} catch(PDOException $e){
    die("ERROR: Could not able to execute $sql. " . $e->getMessage());
}

// Close connection
unset($pdo);