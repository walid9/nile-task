<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["logged_in"]) || $_SESSION["logged_in"] !== true){
    header("location: login.php");
    exit;
}
?>
<html>
<head>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
<div class="container footer">
    <div class="d-flex justify-content-between">
        <div class="p-1">
            <p><?php echo "<b> Welcome ". $_SESSION["username"] ."</b>";?></p>
        </div>
        <div class="p-1">
            <a href="categories/index.php" class="btn btn-warning">Categories</a>
            <a href="sub_categories/index.php" class="btn btn-warning">Sub Categories</a>
            <a href="products/index.php" class="btn btn-warning">Products</a>
            <a href="logout.php" class="btn btn-warning">logout</a>
        </div>
    </div>
</div>
</body>
</html>