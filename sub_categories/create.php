<?php
if($_POST){

    // include database connection
    include '../database/config.php';

    try{

        // insert query
        $query = "INSERT INTO sub_categories SET name=:name, description=:description, category_id=:cat";

        // prepare query for execution
        $stmt = $pdo->prepare($query);

        // posted values
        $name=htmlspecialchars(strip_tags($_POST['name']));
        $description=htmlspecialchars(strip_tags($_POST['description']));
        $cat=htmlspecialchars(strip_tags($_POST['cat_id']));

        // bind the parameters
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':cat', $cat);

        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }

    }

        // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Create Sub Category</title>

    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

</head>
<body>

<!-- container -->
<div class="container">
    <div class="page-header">
        <h1>Create New Sub Category</h1>
    </div>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <table class='table table-hover table-responsive table-bordered'>
            <tr>
                <td>Name</td>
                <td><input type='text' name='name' class='form-control' required></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><textarea name='description' class='form-control' required></textarea></td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <select name="cat_id" class="form-control" required>
                        <?php
                        // include database connection
                        include '../database/config.php';
                        $cats = "SELECT id, name FROM categories ORDER BY id DESC";
                        $stamt = $pdo->prepare($cats);
                        $stamt->execute();
                        while ($row = $stamt->fetch(PDO::FETCH_ASSOC)){
                            extract($row);
                            echo "<option value='{$row['id']}'>{$row['name']}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Action</td>
                <td>
                    <input type='submit' value='Save' class='btn btn-primary' />
                    <a href='index.php' class='btn btn-danger'>Back to categories</a>
                </td>
            </tr>
        </table>
    </form>

</div> <!-- end .container -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
